import "./App.scss";
// import Hero from "./components/Hero/Hero";
import CharacterCardList from "./components/Characters/CharacterCardList/CharacterCardList";
import Hero from "./components/Hero/Hero";
function App() {
  return (
    <>
      <Hero />
      <CharacterCardList />
    </>
  );
}

export default App;
